export const getIframeBody = () =>
  cy
    .get("iframe")
    .invoke("contents")
    .invoke("find", "body");

export const getIframeDom = () => getIframeBody().then(cy.wrap);
