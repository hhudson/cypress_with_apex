// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
Cypress.Commands.add("login", () => {
  const loginPage = "f?p=103:LOGIN_DESKTOP";
  const pUsername = "test_user";
  const pPassword = "password123";
  cy.server();
  cy.route("POST", "/ords/wwv_flow.ajax").as("pageloaded");
  cy.visit(loginPage);
  cy.get(".t-Login-header").should("contain", "Sample Database Application");
  cy.get("#P101_USERNAME")
    .clear()
    .should("be.empty")
    .type(pUsername);
  cy.get("#P101_PASSWORD")
    .clear()
    .should("be.empty")
    .type(pPassword);
  cy.get("[data-cy=sign_inButton]").click();
  cy.wait("@pageloaded");
  cy.url()
    .should("include", "p=103:1:")
    .then($url => {
      window.loggedInPage = $url;
    });
  cy.getCookie("ORA_WWV_APP_103").then($Cookie => {
    window.app_cookie = $Cookie.value;
  });
});

Cypress.Commands.add("waitForModal", () => {
  cy.get("iframe").then($iframe => {
    //wait for iframe to load.
    return new Cypress.Promise(resolve => {
      $iframe.on("load", () => {
        resolve($iframe.contents().find("body"));
        console.log("loaded");
      });
    });
  });
});
