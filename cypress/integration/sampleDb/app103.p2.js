//import { getIframeBody, getIframeDom } from "../../support/utils";

context("App 103 Home page", () => {
  before(function() {
    cy.login();
  });

  beforeEach(function() {
    cy.clearCookies();
    cy.setCookie("ORA_WWV_APP_103", app_cookie);
    var pageUrl = loggedInPage.replace(":1:", ":2:");
    cy.visit(pageUrl);
  });
  it("test interactive report", () => {
    cy.get(".t-Breadcrumb-label").should("contain", "Customers");
    cy.get("#customer_ir_ig_toolbar_search_field").type("Dulles{enter}");
    cy.wait("@pageloaded");
    cy.contains("td", "Dulles").should("be.visible");
  });

  const getIframeDom = () =>
    cy
      .get("iframe")
      .invoke("contents")
      .invoke("find", "body")
      .then(cy.wrap);

  it.only("create customer", () => {
    var fName = "Delete";
    var lName = "Me";
    var zipCode = "02139";
    var credLimit = "1000";
    var state = "Massachusetts";
    cy.get("[data-cy=create_customerButton]").click();
    cy.waitForModal();
    cy.get(".ui-dialog").invoke("css", "height", "500px");
    getIframeDom()
      .find("#P7_CUST_FIRST_NAME")
      .type(fName)
      .should("have.value", fName);
    getIframeDom()
      .find("#P7_CUST_LAST_NAME")
      .type(lName)
      .should("have.value", lName);
    getIframeDom()
      .find("#P7_CUST_POSTAL_CODE")
      .type(zipCode)
      .should("have.value", zipCode);
    getIframeDom()
      .find("#P7_CREDIT_LIMIT")
      .type(credLimit)
      .should("have.value", credLimit);
    getIframeDom()
      .find("#P7_CUST_STATE")
      .select(state)
      .should("have.value", "MA");
    getIframeDom()
      .find("[data-cy=add_customerButton]")
      .click();
    cy.contains("td", fName).should("be.visible");
  });
});
