context("App 103 Login", () => {
  beforeEach(function() {
    cy.visit("f?p=103:LOGIN_DESKTOP");
    cy.get(".t-Login-header").should("contain", "Sample Database Application");
    cy.get("#P101_USERNAME").type("test_user");
  });

  it("plain login", () => {
    cy.server();
    cy.route("POST", "/ords/wwv_flow.ajax").as("pageloaded");
    cy.get("#P101_PASSWORD").type("password123");
    cy.get("[data-cy=sign_inButton]").click();
    cy.wait("@pageloaded");
    cy.url().should("include", "p=103:1:");
  });

  it("fail authentication", () => {
    cy.get("#P101_PASSWORD").type("bad password");
    cy.get("[data-cy=sign_inButton]").click();
    cy.get(".t-Alert-content").should("contain", "Invalid Login Credentials");
  });
});
