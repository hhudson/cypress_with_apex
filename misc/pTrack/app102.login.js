context("App 102 Login", () => {
  beforeEach(function() {
    cy.visit("f?p=102:LOGIN");
    cy.get("#checklist_manager_heading").should("contain", "Checklist Manager");
    cy.get("#P101_USERNAME").type("HAYDEN");
  });

  it("plain login", () => {
    cy.server();
    cy.route("POST", "/ords/wwv_flow.ajax").as("pageloaded");
    cy.get("#P101_PASSWORD").type("password123");
    cy.get("[data-cy=login_btn]").click();
    cy.wait("@pageloaded");
    cy.url().should("include", "p=102:1:");
  });

  it("fail authentication", () => {
    cy.get("#P101_PASSWORD").type("blerg");
    cy.get("[data-cy=login_btn]").click();
    cy.get(".t-Alert-content").should("contain", "Invalid Login Credentials");
  });
});
