import { getIframeBody, getIframeDom } from "../../support/utils";

context("App 102 Home page", () => {
  before(function() {
    cy.login();
  });
  it("test search", () => {
    cy.clearCookies();
    cy.setCookie("ORA_WWV_APP_102", app_cookie);
    var pageUrl = loggedInPage;
    cy.visit(pageUrl);
    cy.get(".t-Card-titleWrap").should("contain", "Go Live");
    cy.get("#P1_SEARCH").type("IT Projects {enter}");
    cy.wait("@pageloaded");
    cy.get(".t-Card-titleWrap").should("contain", "IT projects");
  });

  it("verify report display default", () => {
    cy.clearCookies();
    cy.setCookie("ORA_WWV_APP_102", app_cookie);
    var pageUrl = loggedInPage;
    cy.visit(pageUrl);
    cy.get("#P1_ORDERING").should("have.value", "NAME");
    cy.get(".t-Card-titleWrap").should("contain", "Go Live");
    cy.get("#P1_ORDERING").select("Percent Complete");
    cy.get(".t-Card-titleWrap").should("contain", "IT projects");
  });

  it.only("create checklist item", () => {
    var deleteSeq = "Delete me";
    cy.clearCookies();
    cy.setCookie("ORA_WWV_APP_102", app_cookie);
    var pageUrl = loggedInPage;
    cy.visit(pageUrl);
    cy.get("[data-cy=createChecklist]").click();
    cy.waitForModal();
    getIframeDom()
      .find("#P18_NAME")
      .type(deleteSeq, { force: true })
      .should("have.value", deleteSeq);
    getIframeDom()
      .find("[data-cy=next]")
      .click();
    cy.waitForModal();
    getIframeDom()
      .find("#P19_C01")
      .type(deleteSeq, { force: true });
    getIframeDom()
      .find("[data-cy=next]")
      .click();
    cy.waitForModal();
    getIframeDom()
      .find("#P20_NAME_ATTR_01")
      .type(deleteSeq, { force: true });
    getIframeDom()
      .find("[data-cy=next]")
      .click();
    cy.waitForModal();
    getIframeDom()
      .find("#P21_ROW_01")
      .type(deleteSeq, { force: true });
    getIframeDom()
      .find("[data-cy=next]")
      .click();
    cy.waitForModal();
    getIframeDom()
      .find("[data-cy=create]")
      .click();
    cy.waitForModal();
    getIframeDom()
      .find("[data-cy=view]")
      .click();
    cy.get(".t-HeroRegion-title").should("contain", deleteSeq);
  });
});
